# GitVerse

## Step1: install dependencies

 `npm i`

## Step2: add .env

```bash
cp .env.example .env
```

add your private key and contract address to .env file

## Step3: run hardhat node

### 3.1 create hardhat project
```bash
mkdir project
cd project
npm init --yes
npm install --save-dev hardhat
npx hardhat
```
### 3.2 compile contact
```bash
npx hardhat compile
npx hardhat test
```

### 3.2 deploy contact to hardhat localhost network
copy deploy-gitverse.js to hardhat project
```bash
cp deploy-gitverse.js project/scripts/deploy-gitverse.js
```
```bash
npx hardhat --network localhost run scripts/deploy-gitverse.js
or
npx hardhat --network goerli run scripts/deploy-gitverse.js
```

## Step4: Implement the Gitverse command-line

```bash
gitverse init

gitverse add <pathlist...>

gitverse tag

gitverse commit -m comments
```